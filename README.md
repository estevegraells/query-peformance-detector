This README file is available in English and Spanish.

# README in English

# Introduction

This tool is fully described in this blog entry (only available in Spanish): [Query Performance Detector](https://forcegraells.com/2018/11/09/query-performance-detector).

As our ORG grows, the queries we initially designed (which might be well optimized at the beginning) may suffer performance issues. But not only the SOQL queries written by developers, but as well **List views**, **Reports** and **Dashboards** created by users.

In addition, this issues are born in **silent and unnoticed**, and are only visible with performance issues in production, when end users are already perceaving the issue and the corrective actions (creating indexes, data design, etc.) will not be immediate. This tool enables to obtain the **Salesforce Optimizer** parameters for each ListView, report, and Dashboard executed in the period, obtaining:

1. Cost (Selective < 1 )
1. Rows returned in the query (too many will be unseful but resource and time consuming)
1. The Query Plan decided by the Optimizer (TableScan, Index, Sharing, Other)

It scans the elements in a time period indicated by the user, with the data present in the ORG highlighting those that are not optimal, and so enabling you to focus your efforts on the queries that you certainly know  users are using.

## Dev, Build and Test

Deploy the code of this repo and the 2 objects that are used to store the scan results and internal logs respectively.

## Resources

There are no additional resources used, because even though an additional icons font and the Google charts Library are used, they are already included in the code.

## Description of Files and Directories

In the blog entry are detailed the structure of the Code and the apex classes with visualforce pages as well.

## Contact

[Esteve Graells](https://forcegraells.com)

# README in SPANISH

# Introducción

Esta herramienta se describe en esta entrada de blog: [Query Performance Detector](https://forcegraells.com/2018/11/09/query-performance-detector).

A medida que nuestra ORG crece, las consultas que inicialmente diseñamos (que quizás optimizamos o quizás no) pueden sufrir degradación. Pero no sólo las Queries de los Developers con SOQL, sino de **List Views**, **Reports** y **Dashboards** que crean los usuarios.

Además, esta degradación se realiza en **silencio, inadvertida**, y solo es visible con incidencias de rendimiento en Producción, cuando ya es demasiado tarde, y las acciones correctoras (creación de índices, re-organización de datos, etc.) no serán inmediatas.
Esta herramienta, permite obtener la información del Optimizador de Salesforce para cada ListView, Report y Dashboard ejecutado en el período, obteniendo:

1. Su coste
1. El número de filas retornado
1. El plan de acceso (TableScan, Index, Sharing, Other)

Se analyza el periodo de tiempo indicado, con los datos presentes en la ORG y destaca aquellas que no son óptimas,y así centrar los esfuerzos de optimización en las consultas que realmente los usuarios estan realizando. 

## Dev, Build and Test

Desplegar el código de este repo y 2 objetos para contener los resultados y logs internos respectivamente.

## Resources

No hay recursos adicionales necesarios, ya que aunque se utilizan un fuente adicional de iconos y la librería de Google Charts, estan ya están incluidas en el código.

## Description of Files and Directories

En la entrada del blog se detalla la estructura del código y de clases.

## Contact

[Esteve Graells](https://forcegraells.com)

## Issues

## Demo

![Demo](./images/Demo1.png "Demo")
![Demo](./images/Demo2.png "Demo")
![Demo](./images/Demo3.png "Demo")
![Demo](./images/Demo4.png "Demo")
