/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description POJO to store results
 */

public with sharing class QP_ScanResult {

    public String scanIdentifier                    {get; set;}
    public Datetime scanDate                        {get; set;}
    public String scanDateFormatted                 {get; set;}
    public String type                              {get; set;}
    public Integer totalQueriesAnalyzed             {get; set;}
    public Integer totalQueriesNonSelective         {get; set;}
    public Integer totalQueriesOperationIndex       {get; set;}
    public Integer totalQueriesOperationTableScan   {get; set;}
    public Integer totalQueriesOperationOther       {get; set;}
    public Integer totalQueriesOperationSharing     {get; set;}

    public QP_ScanResult() {

    }
}
