/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get the last dashboards executed, then get the reports
 * of its components
 */
public with sharing class QP_GetLastDashboards {
    public QP_GetLastDashboards(Integer days) {

        lookPastNumDays = days;
        Logger.insertLogEntry('Start scanning for Last Dashboards ' + Datetime.newInstance(System.now().date(), System.now().time()));

    }

    //@description Look in the past lookPastNumDays to get List Views executed
    public Integer lookPastNumDays = 0;

    /*******************************************************************************************************
    * @description get Last Viewed List Viewed Run in the last N days
    * @param none
    * @return the list of list viewed
    */
    public String getLastRunReportsFromDashboards(){

        String lastExecutedReportsFromDashboardsQ =
            ' SELECT Id, (SELECT CustomReportId FROM DashboardComponents)' + 
            ' FROM Dashboard ' +
            ' WHERE LastViewedDate = LAST_N_DAYS:' + lookPastNumDays +
            ' ORDER BY LastViewedDate DESC';

        Datetime now = datetime.now();
		String runIdentifier = now.format('YYYYMMDDHHmmss');

        try{
            List<Dashboard> latestDashboardsExecuted = Database.query(lastExecutedReportsFromDashboardsQ);

            List<QueryCostHistory__c> pendingDCsToAnalyze = new List<QueryCostHistory__c>();

            for (Dashboard d : latestDashboardsExecuted){

                for (DashboardComponent dc : d.DashboardComponents){

                    QueryCostHistory__c newPendingDC = new QueryCostHistory__c( 
                        Query_ID__c     = dc.CustomReportId,
                        QueryType__c    = 'Dashboard Query',
                        Status__c       = 'Pending',
                        Scan_Identifier__c = runIdentifier
                    );

                    pendingDCsToAnalyze.add(newPendingDC);
                }
            }

            //Insert those DCs as Pending Plans
            insert pendingDCsToAnalyze;
            System.debug('-ege- ' + pendingDCsToAnalyze.size() + ' reports from dashboards to Analyze');
                    
        }catch (Exception e){
            System.debug( '-ege- Exception: ' + e.getMessage() );
        }

        return runIdentifier;
    }
}