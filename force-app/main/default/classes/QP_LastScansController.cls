/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get the list of last scans run
 */
public with sharing class QP_LastScansController {

	public Map<String, QP_ScanResult> lastScansResults {get; set;}

	public QP_LastScansController() {

		System.debug('-ege- Constructor');

		lastScansResults = new Map<String, QP_ScanResult>();

		//Get all queries per each scan
		for (AggregateResult ar : [
				SELECT Scan_Identifier__c scanid, count(id) totalQueries
				FROM QueryCostHistory__c
				GROUP BY Scan_Identifier__c
				]){

			QP_ScanResult scan = new QP_ScanResult();
			scan.scanIdentifier         = String.valueOf(ar.get('scanid'));
			scan.totalQueriesAnalyzed   = Integer.valueOf(ar.get('totalQueries'));

			lastScansResults.put(scan.scanIdentifier, scan);
		}

		System.debug('-ege- lastScansResults: ' + lastScansResults);

		//Get all queries non-selective per each scan
		for (AggregateResult ar : [
				SELECT Scan_Identifier__c scanid, count(id) totalQueriesNonSelective
				FROM QueryCostHistory__c
				WHERE Cost__c > 1
				GROUP BY Scan_Identifier__c]) {

			String scanId = String.valueOf((ar.get('scanid')));
			lastScansResults.get(scanId).totalQueriesNonSelective = Integer.valueOf(ar.get('totalQueriesNonSelective'));
		}

		//Get counters per optimizer leading operation
		for (AggregateResult ar : [
				SELECT Scan_Identifier__c scanid, Leading_Operation__c op, count(id) total
				FROM QueryCostHistory__c
				GROUP BY Scan_Identifier__c, Leading_Operation__c]) {

			String scanId 		= String.valueOf((ar.get('scanid')));
			String operation 	= String.valueof(ar.get('op'));
			Integer total 		= Integer.valueOf(ar.get('total'));

			switch on operation {
				when 'Index' {
					lastScansResults.get(scanId).totalQueriesOperationIndex = total;
				}
				when 'TableScan' {
					lastScansResults.get(scanId).totalQueriesOperationTableScan = total;
				}
				when 'Sharing' {
					lastScansResults.get(scanId).totalQueriesOperationSharing = total;
				}
				when 'Other' {
					lastScansResults.get(scanId).totalQueriesOperationOther = total;
				}
			}
		}

		//get Scan Dates 
		List<QueryCostHistory__c> scanDates = [
			SELECT createdDate, Scan_Identifier__c
			FROM QueryCostHistory__c 
			WHERE Scan_Identifier__c IN :lastScansResults.keyset()];

		System.debug('-ege- scanDates: ' + scanDates);

		if (scanDates.size()>0){

			for( String scanId : lastScansResults.keySet() ){

				lastScansResults.get(scanId).scanDateFormatted = getScanDate(scanDates, scanId).format('YYYY-MM-dd HH:mm');
			}
		}
		System.debug('-ege- lastScansResults: ' + lastScansResults);


		//get Type 
		List<QueryCostHistory__c> types = [
			SELECT QueryType__c, Scan_Identifier__c
			FROM QueryCostHistory__c 
			WHERE Scan_Identifier__c IN :lastScansResults.keyset()];

		System.debug('-ege- scanDates: ' + scanDates);

		if (types.size()>0){

			for( String scanId : lastScansResults.keySet() ){

				lastScansResults.get(scanId).type = getType(types, scanId); ///SOQL, ListView, Report, Dashboard 
			}
		}
	}

	private Datetime getScanDate(List<QueryCostHistory__c> scanDates, String id){
		
		Datetime scanDate;
		for ( QueryCostHistory__c scanRes : scanDates ){

			if (scanRes.Scan_Identifier__c == id){
				scanDate = scanRes.createdDate;
			}	
		}
		return scanDate;
	}

	private String getType(List<QueryCostHistory__c> types, String id){
		
		String type = '';
		for ( QueryCostHistory__c scanRes : types ){

			if (scanRes.Scan_Identifier__c == id){
				type = scanRes.QueryType__c.substringBefore(' '); //Type is 'SOQL Query' or 'ListView Query', we need just the first part
			}	
		}
		return type;
	}
}
