/**
* @author Esteve Graells
* @date October 2018
*
* @description Main Controller to route to specific controllers
*/

public with sharing class QP_MainController {

	public Integer daysToCheckForReports            {get; set; }
	public Integer daysToCheckForListViews          {get; set; }
	public Integer daysToCheckForDashboards         {get; set; }
	public Integer daysToCheckForSoqlQs				{get; set; }

	public Integer exactDaysToCheckForListViews     {get; set; }
	public Integer exactDaysToCheckForReports		{get; set; }
	public Integer exactDaysToCheckForDashboards    {get; set; }
	public Integer exactDaysToCheckForSoqlQs		{get; set; }

	public static Integer analyzedListViews			{get; set;}
	public static Integer analyzedReports			{get; set;}
	public static Integer analyzedDashboards		{get; set;}
	public static Integer queriesFound				{get; set;}

	public static Integer analyzedListViewsWithNonSelectiveCost     {get; set;}
	public static Integer analyzedReportsWithNonSelectiveCost     	{get; set;}
	public static Integer analyzedDashboardsWithNonSelectiveCost	{get; set;}
	public static Integer analyzedSoqlQsWithNonSelectiveCost		{get; set;}

	public Datetime lastScanDateForListViews        {get; set;}
	public Datetime lastScanDateForReports          {get; set;}
	public Datetime lastScanDateForDashboards       {get; set;}
	public Datetime lastScanDateForSoqlQs			{get; set;}

	public Boolean getLastRunViewsRun               {get; set;}
	public Boolean getLastRunReportsRun				{get; set;}
	public Boolean getLastRunDashboardsRun			{get; set;}
	public Boolean getLastRunSoqlQsRun				{get; set;}

	public String statusListViewBatchCompleted		{get; set;}
	public String statusReportBatchCompleted		{get; set;}
	public String statusDashboardBatchCompleted		{get; set;}
	public String statusLogBatchCompleted			{get; set;}
	public String statusSoqlQBatchCompleted			{get; set;}
	
	public Id batchListViewId                       {get; set;}
	public Id batchReportId							{get; set;}
	public Id batchDashboardId						{get; set;}
	public Id batchLogId							{get; set;}
	
	public String listViewRunIdentifier				{get; set;}
	public String reportRunIdentifier				{get; set;}
	public String dashboardRunIdentifier			{get; set;}
	public String logScanIdentifier					{get; set;}

	//Init the world
	public QP_MainController() {

		daysToCheckForListViews     	= 1;
		daysToCheckForReports       	= 1;
		daysToCheckForDashboards    	= 1;
		daysToCheckForSoqlQs          	= 1;

		exactDaysToCheckForListViews 	= 0;
		exactDaysToCheckForReports 		= 0;
		exactDaysToCheckForDashboards 	= 0;
		exactDaysToCheckForSoqlQs		= 0;

		getLastRunViewsRun 				= false;
		getLastRunReportsRun 			= false;
		getLastRunDashboardsRun 		= false;
		getLastRunSoqlQsRun		 		= false;

		//Show last results from the last scan executed for the 4 types
		lastScanDateForListViews 	= [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'ListView Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;
		lastScanDateForReports 		= [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'Report Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;
		lastScanDateForDashboards 	= [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'Dashboard Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;
		lastScanDateForSoqlQs 		= [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'SOQL Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;
	}

	//Get the Last Views Executed and call the API to get the Query Plans
	public void runJobLastListViews(){
		try{

			statusListViewBatchCompleted = 'Starting Query Performance Engine...';

			if (exactDaysToCheckForListViews!=0) {
				daysToCheckForListViews = exactDaysToCheckForListViews;
			}

			QP_GetLastViewedListViews listviews = new QP_GetLastViewedListViews(daysToCheckForListViews);
			listViewRunIdentifier = listviews.getLastRunViews();

			QP_ToolingAPIRequester requester = new QP_ToolingAPIRequester();
			batchListViewId = Database.executeBatch(requester, 10);

			getLastRunViewsRun = true;

		}catch (Exception e) {
			System.debug('-ege- Error: ' + e.getMessage());
		}
	}

	//Get the Last Reports Executed and call the API to get the Query Plans
	public void runJobLastReports(){
		try{

			statusReportBatchCompleted = 'Starting Query Performance Engine...';

			if (exactDaysToCheckForReports!=0) {
				daysToCheckForReports = exactDaysToCheckForReports;
			}

			System.debug('-ege- daysToCheckForReports:' + daysToCheckForReports);

			QP_GetLastRunReports reports = new QP_GetLastRunReports(daysToCheckForReports);
			reportRunIdentifier = reports.getLastRunReports();

			QP_ToolingAPIRequester requester = new QP_ToolingAPIRequester();
			batchReportId = Database.executeBatch(requester, 10);

			getLastRunReportsRun = true;

		}catch (Exception e) {
			System.debug('-ege- Error: ' + e.getMessage());
		}
	}

	//Get the Last Dashboards Executed and call the API to get the Query Plans for its components
	public void runJobLastDashboards(){
		try{

			statusDashboardBatchCompleted = 'Starting Query Performance Engine...';

			if (exactDaysToCheckForDashboards!=0) {
				daysToCheckForDashboards = exactDaysToCheckForDashboards;
			}

			System.debug('-ege- daysToCheckForDashboards:' + daysToCheckForDashboards);

			QP_GetLastDashboards dashboards = new QP_GetLastDashboards(daysToCheckForDashboards);
			dashboardRunIdentifier = dashboards.getLastRunReportsFromDashboards();

			QP_ToolingAPIRequester requester = new QP_ToolingAPIRequester();
			batchDashboardId = Database.executeBatch(requester, 10);

			getLastRunDashboardsRun = true;

		}catch (Exception e) {
			System.debug('-ege- Error: ' + e.getMessage());
		}
	}

	//Get the Last Logs and extract SOQL Queries on them
	public void runJobAnalyzeLogs(){
		try{

			statusLogBatchCompleted = 'Starting Query Performance Engine...';

			if (exactDaysToCheckForSoqlQs!=0) {
				daysToCheckForSoqlQs = exactDaysToCheckForSoqlQs;
			}

			System.debug('-ege- daysToCheckForSoqlQs:' + daysToCheckForSoqlQs);

			Datetime now = datetime.now();
			logScanIdentifier = now.format('YYYYMMDDHHmmss');

			//Call to get last SOQLs run on the period
			QP_checkUserLogsForSOQLQueries SOQLQueries = new QP_checkUserLogsForSOQLQueries(daysToCheckForSoqlQs, logScanIdentifier);
			batchLogId = Database.executeBatch(SOQLQueries, 10);

			getLastRunSoqlQsRun = true;

		}catch (Exception e) {
			System.debug('-ege- Error: ' + e.getMessage());
		}
	}

	//Update the User Interface status
	public Pagereference batchJobListViewStatus(){

		if (batchListViewId != null && statusListViewBatchCompleted != 'Completed') {

			statusListViewBatchCompleted = [SELECT Status FROM AsyncApexJob WHERE Id = :batchListViewId LIMIT 1].Status;

			if (statusListViewBatchCompleted == 'Completed') {
				analyzedListViews = getCountAnalyzed('ListView');
				analyzedListViewsWithNonSelectiveCost = getCountNonSelectiveFound('ListView');
			}
		}
		return null;
	}

	//Update the User Interface status
	public Pagereference batchJobReportStatus(){

		if (batchReportId != null && statusReportBatchCompleted != 'Completed') {

			statusReportBatchCompleted = [SELECT Status FROM AsyncApexJob WHERE Id = :batchReportId LIMIT 1].Status;

			if (statusReportBatchCompleted == 'Completed') {
				analyzedReports = getCountAnalyzed('Report');
				analyzedReportsWithNonSelectiveCost = getCountNonSelectiveFound('Report');
			}
		}
		return null;
	}

	//Update the User Interface status
	public Pagereference batchJobDashboardStatus(){

		if (batchDashboardId != null && statusDashboardBatchCompleted != 'Completed') {

			statusDashboardBatchCompleted =  [SELECT Status FROM AsyncApexJob WHERE Id = :batchDashboardId LIMIT 1].Status;

			if (statusDashboardBatchCompleted == 'Completed') {
				analyzedDashboards = getCountAnalyzed('Dashboard');
				analyzedDashboardsWithNonSelectiveCost = getCountNonSelectiveFound('Dashboard');
			}
		}
		return null;
	}

	//Update the User Interface status
	public Pagereference batchLogStatus(){

		if (batchLogId != null && statusLogBatchCompleted != 'Completed') {

			statusLogBatchCompleted = [SELECT Status FROM AsyncApexJob WHERE Id = :batchLogId LIMIT 1].Status;
		}
		
		if (statusLogBatchCompleted == 'Completed') {

			String countQuery =
			'SELECT count() ' +
			'FROM QueryCostHistory__c ' +
			'WHERE Status__c = \'Analyzed\' AND QueryType__c = \'SOQL Query\' AND Scan_Identifier__c = \'' + logScanIdentifier + '\'';

			queriesFound =  Database.countQuery(countQuery);
			System.debug('-ege- Queries Found: ' + queriesFound);
		}
		return null;
	}

	//Get total counts
	public Integer getCountAnalyzed(String type){

		String fullType = '';
		String scanId = '';

		switch on type{
			when 'ListView'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = listViewRunIdentifier;
			}

			when 'Report'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = reportRunIdentifier;
			}

			when 'Dashboard'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = dashboardRunIdentifier;
			}
		}

		String countQuery =
			'SELECT count() ' +
			'FROM QueryCostHistory__c ' +
			'WHERE Status__c = \'Analyzed\' AND QueryType__c =' + fullType + ' AND Scan_Identifier__c = \'' + scanId + '\'';

		System.debug('-ege- countQuery: ' + countQuery);

		return Database.countQuery(countQuery);
	}


	//Get counts for non-selective results
	public Integer getCountNonSelectiveFound(String type){

		String fullType = '';
		String scanId = '';

		switch on type{
			when 'ListView'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = listViewRunIdentifier;
			}

			when 'Report'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = reportRunIdentifier;
			}

			when 'Dashboard'{
				fullType = '\'' + type + ' Query'+ '\''; 
				scanId = dashboardRunIdentifier;
			}
		}

		String countNonSelective =
			'SELECT count() ' +
			'FROM QueryCostHistory__c ' +
			'WHERE Status__c = \'Analyzed\' AND QueryType__c = ' + fullType + ' AND Cost__c > 1 AND Scan_Identifier__c = \'' + scanId + '\'';

		return Database.countQuery(countNonSelective);
	}

	public Pagereference goLastScansPage(){

		Pagereference lastScansPage = Page.QP_LastScans;
		lastScansPage.setRedirect(true);
		return lastScansPage;
	}

	
	public Pagereference goShowListViewDetailedPage(){

		Pagereference resultsDetailedPage = Page.QP_ShowDetailedPage;
		resultsDetailedPage.setRedirect(true);
		resultsDetailedPage.getParameters().put('scanid', listViewRunIdentifier);
		resultsDetailedPage.getParameters().put('type', 'ListView');
		return resultsDetailedPage;
	}

	public Pagereference goShowReportDetailedPage(){

		Pagereference resultsDetailedPage = Page.QP_ShowDetailedPage;
		resultsDetailedPage.setRedirect(true);
		resultsDetailedPage.getParameters().put('scanid', reportRunIdentifier);
		resultsDetailedPage.getParameters().put('type', 'Report');
		return resultsDetailedPage;
	}

	public Pagereference goShowDashboardDetailedPage(){

		Pagereference resultsDetailedPage = Page.QP_ShowDetailedPage;
		resultsDetailedPage.setRedirect(true);
		resultsDetailedPage.getParameters().put('scanid', dashboardRunIdentifier);
		resultsDetailedPage.getParameters().put('type', 'Dashboard');
		return resultsDetailedPage;
	}

	public Pagereference goShowLogDetailedPage(){

		Pagereference resultsDetailedPage = Page.QP_ShowDetailedPage;
		resultsDetailedPage.setRedirect(true);
		resultsDetailedPage.getParameters().put('scanid', logScanIdentifier);
		resultsDetailedPage.getParameters().put('type', 'SOQL');
		return resultsDetailedPage;
	}

	/* public void getLastReports(){
		QP_GetLastRunReports reports = new QP_GetLastRunReports(daysToCheckForReports);
		reports.getLastRunReports();

		QP_ToolingAPIRequester requester = new QP_ToolingAPIRequester();
		Database.executeBatch(requester, 10);

		lastScanDateForReports = [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'Report Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;
	}

	public void getLastDashboards(){
		QP_GetLastDashboards dashboards = new QP_GetLastDashboards(daysToCheckForDashboards);
		dashboards.getLastRunReportsFromDashboards();

		QP_ToolingAPIRequester requester = new QP_ToolingAPIRequester();
		Database.executeBatch(requester, 10);

		lastScanDateForDashboards   = [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'Dashboard Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;

	}

	public void getLastSOQLQueriesFromLogs(){
		QP_checkUserLogsForSOQLQueries SOQLQueries = new QP_checkUserLogsForSOQLQueries(daysToCheckForLogs, usernameUserLogs);
		Database.executeBatch(SOQLQueries, 10);
		lastScanDateForLogs = [SELECT CreatedDate FROM QueryCostHistory__c WHERE QueryType__c = 'SOQL Query' ORDER BY CreatedDate DESC LIMIT 1].CreatedDate;

		//No need to call QP_ToolingAPIRequester as QP_checkUserLogsForSOQLQueries is a batchable class that chains the requester

	} */

}