/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to etailed info of a scan
 */
public with sharing class QP_ShowDetailedController {

	public List<QP_DetailedResult> elemsDetailedList {get; set; }

	public QP_ShowDetailedController() {

		elemsDetailedList = new List<QP_DetailedResult>();

		String scanId = ApexPages.currentPage().getParameters().get('scanid');
		String elemType = ApexPages.currentPage().getParameters().get('type');

		System.debug('-ege- elemtype is: ' + elemType);

		List<QueryCostHistory__c> resultsFromScan;

		if (scanId == 'all') {
			//Get all results
			resultsFromScan = [
				SELECT CreatedDate, Query_ID__c, QueryType__c, Cardinality__c, Cost__c, Leading_Operation__c, Optimizer_Description__c, Scan_Identifier__c
				FROM QueryCostHistory__c
				ORDER BY CreatedDate DESC
			];

		}else{

			if (elemType == 'SOQL') {
				//Get results for queries, don't have cost, cardinality, etc.
				resultsFromScan = [
					SELECT CreatedDate, Query_ID__c, Query_Statement__c, QueryType__c, Scan_Identifier__c
					FROM QueryCostHistory__c
					WHERE Scan_Identifier__c = :scanId 
					ORDER BY Cost__c DESC
				];

			}else{

				//Get results for this scan
				resultsFromScan = [
					SELECT CreatedDate, Query_ID__c, Query_Statement__c, QueryType__c, Cardinality__c, Cost__c, Leading_Operation__c, Optimizer_Description__c, Scan_Identifier__c
					FROM QueryCostHistory__c
					WHERE Scan_Identifier__c = :scanId AND Cost__c != 0 AND Cost__c != NULL
					ORDER BY Cost__c DESC
				];
			}
		System.debug('-ege- resultat de la query es: ' + resultsFromScan);
		}

		//Get Ids to get some attributes from ListView object
		List<Id> resultsIds = new List<Id>();

		if (resultsFromScan.size()>0) {

			for (QueryCostHistory__c result : resultsFromScan) {
				resultsIds.add((ID)result.Query_ID__c);
			}			
		}

		switch on elemType{

			when 'ListView' {
				Map<Id, ListView> listViewAttribs = new Map<ID, ListView>([SELECT Name, SObjectType FROM ListView WHERE Id IN: resultsIds]);

				//Create the list to send to the Page
				for (QueryCostHistory__c result :  resultsFromScan) {

					QP_DetailedResult detailedResult = new QP_DetailedResult();
					detailedResult.createdDate     	= result.createdDate;
					detailedResult.id              	= result.Query_ID__c;
					detailedResult.cardinality     	= Integer.valueof(result.Cardinality__c);
					detailedResult.cost            	= result.Cost__c;
					detailedResult.operation       	= result.Leading_Operation__c;
					detailedResult.description     	= result.Optimizer_Description__c;
					String objectName				= listViewAttribs.get(result.Query_ID__c).SObjectType;
					detailedResult.relatedObject   	= objectName;
					detailedResult.name			   	= listViewAttribs.get(result.Query_ID__c).Name + ' on ' + objectName;						
					detailedResult.url				= '/' + getPrefixObjectName(objectName) + '?fcf=' + result.Query_ID__c;
					
					elemsDetailedList.add(detailedResult);
				}
			}
			
			when 'Report' {
				Map<Id, Report> reportAttribs = new Map<ID, Report>([SELECT Name FROM Report WHERE Id IN: resultsIds]);

				//Create the list to send to the Page
				for (QueryCostHistory__c result :  resultsFromScan) {

					QP_DetailedResult detailedResult 	= new QP_DetailedResult();
					detailedResult.createdDate     		= result.createdDate;
					detailedResult.id              		= result.Query_ID__c;
					detailedResult.cardinality     		= Integer.valueof(result.Cardinality__c);
					detailedResult.cost            		= result.Cost__c;
					detailedResult.operation       		= result.Leading_Operation__c;
					detailedResult.description     		= result.Optimizer_Description__c;
					detailedResult.name			   		= reportAttribs.get(result.Query_ID__c).Name;						
					detailedResult.url					= '/' + result.Query_ID__c;
					
					elemsDetailedList.add(detailedResult);
				}
			}

			//TODO: segurament podem ajuntar Report y Dashboard
			when 'Dashboard' {

				Map<Id, Report> reportAttribs = new Map<ID, Report>([SELECT Name FROM Report WHERE Id IN: resultsIds]);

				//Create the list to send to the Page
				for (QueryCostHistory__c result :  resultsFromScan) {

					QP_DetailedResult detailedResult 	= new QP_DetailedResult();
					detailedResult.createdDate     		= result.createdDate;
					detailedResult.id              		= result.Query_ID__c;
					detailedResult.cardinality     		= Integer.valueof(result.Cardinality__c);
					detailedResult.cost            		= result.Cost__c;
					detailedResult.operation       		= result.Leading_Operation__c;
					detailedResult.description     		= result.Optimizer_Description__c;
					detailedResult.name			   		= reportAttribs.get(result.Query_ID__c).Name;						
					detailedResult.url					= '/' + result.Query_ID__c;
					
					elemsDetailedList.add(detailedResult);
				}
			}

			//TODO: segurament podem ajuntar Report y Dashboard
			when 'SOQL' {

				System.debug('-ege- Entro a SOQL i els resultats no haurien dhaver variat: ' + resultsFromScan);

				//Create the list to send to the Page
				for (QueryCostHistory__c result :  resultsFromScan) {

					QP_DetailedResult detailedResult 	= new QP_DetailedResult();
					detailedResult.createdDate     		= result.createdDate;
					detailedResult.id              		= result.Query_ID__c;
					detailedResult.queryStatement		= result.Query_Statement__c;
					detailedResult.cardinality     		= 0;
					detailedResult.cost            		= 0;
					detailedResult.operation       		= '';
					detailedResult.description     		= '';
					detailedResult.name			   		= '';
					
					elemsDetailedList.add(detailedResult);
				}
			}
		}

		System.debug('-ege- elemsDetailedList: ' + elemsDetailedList);
	}

	public String getPrefixObjectName(String objectName){

		Map<String,String> prefixList = new Map<String,String>{};
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

		for(String sObj : gd.keySet()) {
			Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
			prefixList.put(r.getName(), r.getKeyPrefix());
		}
		return prefixList.get(objectName);
	}
}
