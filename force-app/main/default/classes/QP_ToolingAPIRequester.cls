/**
* @author Esteve Graells
* @date October 2018
*
* @description Allows calling the Tooling API to get Query Plans
* It is a Batchable class that will be called in chunks of 10, allowing not
* to saturate the Flex Queue
*/


global with sharing class QP_ToolingAPIRequester implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    private final static String serviceURLEndpoint = '/services/data/v43.0/tooling/query/?explain=';
    
    public static List<QueryCostHistory__c> analyzedPlans = new List<QueryCostHistory__c>();
    
    public QP_ToolingAPIRequester() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String getAllPendingPlans = 
            'SELECT Id, Query_ID__c, QueryType__c, Query_Statement__c ' + 
            'FROM QueryCostHistory__c '+
            'WHERE Status__c = \'Pending\''
            ;
        
        //Scope is limited to 10, by the scope parameter used on Database.ExecuteBatch to avoid callout limits
        return Database.getQueryLocator (getAllPendingPlans);
    }

    global void execute(Database.BatchableContext BC, List<QueryCostHistory__c> scope){

        System.debug('-ege- execute' );

        for (QueryCostHistory__c pendingPlan : scope){

            System.debug('-ege- Analyzing plan: ' + pendingPlan);

            HttpRequest httpRequest = new HttpRequest();
            httpRequest.setMethod('GET');
            httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
            httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            HttpRequest.setCompressed(true);

            if (pendingPlan.QueryType__c == 'SOQL Query'){
                httpRequest.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + serviceURLEndpoint + pendingPlan.Query_Statement__c);     
            }else{
                httpRequest.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + serviceURLEndpoint + pendingPlan.Query_ID__c); 
            }

            try {
                Http http = new Http();
                HttpResponse httpResponse = http.send(httpRequest);
                
                if (httpResponse.getStatusCode() == 200 ) {
                    String response = httpResponse.getBody();

                    Map<String, Object> queryPlansObtained = (Map<String, Object>) JSON.deserializeUntyped(response);
                    Map<String, Object> firstPlan = new Map<String, Object>();

                    if (queryPlansObtained!=null) {
                        firstPlan = gestFirstPlan(queryPlansObtained);
                    }else{
                        //Logger.insertLogEntry('No plans obtained :-(');
                        System.debug('-ege- No plans obtained :-( ');
                    }

                    System.debug('-ege- firstPlan: ' + firstPlan);

                    //This pending plan is analyzed, and results updated
                    QueryCostHistory__c analyzedPlan = new QueryCostHistory__c(

                        Id                          = pendingPlan.Id,
                        Query_Statement__c          = pendingPlan.Query_Statement__c,
                        Status__c                   = 'Analyzed',
                        Cardinality__c              = Integer.valueof(firstPlan.get('cardinality')),
                        Cost__c                     = Double.valueof(firstPlan.get('relativeCost')),
                        Leading_Operation__c        = String.valueof(firstPlan.get('leadingOperationType')),
                        Optimizer_Description__c    = String.valueof(firstPlan.get('planDescription'))
                    );

                    analyzedPlans.add(analyzedPlan);

                    //System.debug('-ege- Plan added to list. List contains: ' + analyzedPlans.size());

                    //Logger.insertLogEntry('Plan upated: ' + analyzedPlan);
                    
                } else {
                    
                    QueryCostHistory__c analyzedPlanWithError = new QueryCostHistory__c(

                        Id          = pendingPlan.Id,
                        Status__c   = 'Error ocurred when calling API ' + httpResponse.getStatusCode()
                    );

                    analyzedPlans.add(analyzedPlanWithError); 
                }
            
            } catch( System.Exception e) {
                //Logger.insertLogEntry('Callout exception: ' + e);
                System.debug('-ege- Callout exception: ' + e);
            }
            
        }

        Database.update(analyzedPlans);
        Logger.insertLogEntry('Plans updated: ' + analyzedPlans.size());
    }

    //TODO: aquesta funció es crida quan tots els chunks de 10 s'han executat
    //podria servir per llençar un event indicant que s'ha acabat de completar la tasca complerta
    global void finish(Database.BatchableContext BC){

        Logger.insertLogEntry('Partial Batch finished ');

    } 

    /*******************************************************************************************************
    * @description Even though the optimizer will give us all query plans for a single query, we just one the 
    * best one, which is the first.
    * @param none
    * @return 
    */
    private Map<String, Object> gestFirstPlan(Map<String, Object> queryPlansObtained){

        //check if the sourcequery exists   
        String sourceQuery = null;
        String planDescription = '';
        Map<String, Object> firstPlan = new Map<String, Object>();

        if (queryPlansObtained.containsKey('sourceQuery')){

            sourceQuery = (String)queryPlansObtained.get('sourceQuery');
            firstPlan.put('sourceQuery', sourceQuery);

        }else{
            //Logger.insertLogEntry('Something got wrong - no query plans found');
            System.debug('-ege- Something got wrong - no query plans found');
        }

        if (queryPlansObtained.size()>0){
            
            //get first query plan which is the best one "plans" array
            List<Object> plansResults = (List<Object>)queryPlansObtained.get('plans');

            if (plansResults.size()>0) {
                
                firstPlan = (Map<String, Object>)plansResults.get(0);
                List<Object> firstPlanNotes = new List<Object>();

                //Not always notes node is fullfiled
                if (firstPlan != null) {

                    firstPlanNotes = (List<Object>)firstPlan.get('notes');

                    //Not always notes node is fullfiled
                    if (!firstPlanNotes.isEmpty()) {

                        Map<String, Object> firstPlanDescription = (Map<String, Object>)firstPlanNotes[0];

                        if (firstPlanDescription != null){

                            planDescription = String.valueof(firstPlanDescription.get('description'));
                            firstPlan.put('planDescription', planDescription);

                        } 
                    }
                }
            }
        }else{
            //Logger.insertLogEntry('No query plans for Plan: ' + sourceQuery);
            System.debug('-ege- No query plans for Plan: ' + sourceQuery);
        }

        return firstPlan;
        
    }
}