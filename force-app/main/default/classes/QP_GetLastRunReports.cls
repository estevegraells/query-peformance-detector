/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get the last Reports run on the period
 */
public with sharing class QP_GetLastRunReports {

    private Integer daysToCheck = 0;

    public QP_GetLastRunReports( Integer days) {

        daysToCheck = days;
        Logger.insertLogEntry('Start scanning for Last Run Reports ' + Datetime.newInstance(System.now().date(), System.now().time()));
    }
     
    //Returns Reports Run in the last N days
    public String getLastRunReports(){

        String queryLastReports =
            ' SELECT Id, LastRunDate' + 
            ' FROM Report ' +
            ' WHERE LastRunDate = LAST_N_DAYS:' + daysToCheck +
            ' ORDER BY LastRunDate DESC';

        Datetime now = datetime.now();
		String runIdentifier = now.format('YYYYMMDDHHmmss');
        
        try{
            List<Report> reportsRun = Database.query(queryLastReports);

            System.debug('-ege- ' + reportsRun.size() + ' reports to Analyze');

            //Insert those reports as Pending Plans
            List<QueryCostHistory__c> pendingReportsToAnalyze = new List<QueryCostHistory__c>();
            for (Report r: reportsRun){

                QueryCostHistory__c newPendingReport = new QueryCostHistory__c( 
                    Query_ID__c = r.Id,
                    QueryType__c = 'Report Query',
                    Status__c = 'Pending',
                    Scan_Identifier__c = runIdentifier
                );

                pendingReportsToAnalyze.add(newPendingReport);
            }

            insert pendingReportsToAnalyze;

        }catch (Exception e){
            System.debug( '-ege- Exception: ' + e.getMessage() );
        }

        return runIdentifier;
    }
}
