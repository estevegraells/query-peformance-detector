/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description POJO to store Detailed results info
 */
public with sharing class QP_DetailedResult {

    public ID id  {get; set; }
    public Datetime createdDate  {get; set; }
    public String createdDateFormatted  {get; set; }
    public Integer cardinality {get; set; }
    public Double cost {get; set; }
    public String operation {get; set; }
    public String description {get; set; }
    public String name {get; set; }
    public String relatedObject {get; set; }
    public String url {get; set;}
    public String querytype {get; set;}
    public String queryStatement {get; set;}

    public QP_DetailedResult() {

    }
}
