/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get last results for trending reporting
 */

public with sharing class QP_ShowTrendingResultsController {

	public static List<QP_DetailedResult> queryResultsHistory {get; set; }

	public QP_ShowTrendingResultsController() {

		queryResultsHistory = new List<QP_DetailedResult>();

		String queryId = ApexPages.currentPage().getParameters().get('id').escapeHtml4();
		String queryName = ApexPages.currentPage().getParameters().get('name').escapeHtml4();
		String queryObject = ApexPages.currentPage().getParameters().get('object').escapeHtml4();

		if (queryId != '' && queryName != NULL && queryObject != NULL ) {

			getAllScansForQuery(queryId);
		}
	}

	@RemoteAction
	public static List<QP_DetailedResult> getAllScansForQuery(String queryId){

		queryResultsHistory = new List<QP_DetailedResult>();

		if ( queryId != '' ) {

			//Get resutls for a concrete Query
			List<QueryCostHistory__c> resultsHistoryQuery = [
				SELECT CreatedDate, Query_ID__c, Cardinality__c, Cost__c, Leading_Operation__c, Optimizer_Description__c, Scan_Identifier__c
				FROM QueryCostHistory__c
				WHERE Query_ID__c = :queryId
				ORDER BY CreatedDate DESC
			];

			//Create the list to send to the Page
			for (QueryCostHistory__c result :  resultsHistoryQuery) {
				QP_DetailedResult detailedResult = new QP_DetailedResult();
				detailedResult.createdDate     = result.createdDate;
				detailedResult.createdDateFormatted     = result.createdDate.format('yyyy-MM-dd HH:mm');
				detailedResult.id              = result.Query_ID__c;
				detailedResult.cardinality     = Integer.valueof(result.Cardinality__c);
				detailedResult.cost            = result.Cost__c;
				detailedResult.operation       = result.Leading_Operation__c;
				detailedResult.description     = result.Optimizer_Description__c;

				queryResultsHistory.add(detailedResult);
			}

			System.debug('-ege- queryId: ' + queryId);
			System.debug('-ege- queryResultsHistory :' + queryResultsHistory);

		}else{
			System.debug('-ege- Wierd...');
		}

		return queryResultsHistory;
	}

}
