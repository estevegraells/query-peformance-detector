/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get logs in the system and find SOQL Queries inside
 */

global with sharing class QP_checkUserLogsForSOQLQueries implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    private final String serviceURLEndpoint = '/services/data/v43.0/tooling/sobjects/Apexlog/';

    public Integer lookPastNumDays = 0; 
    private String scanIdentifier = '';

    public QP_checkUserLogsForSOQLQueries(Integer days, String scanId){
        
        lookPastNumDays = days;
        scanIdentifier = scanId;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Date daysAgo = system.today().addDays(-lookPastNumDays);

        String userLogsQ = 
            'SELECT Id ' +
            'FROM ApexLog ' +  
            'WHERE LastModifiedDate = LAST_N_DAYS:' + lookPastNumDays;
        
        System.debug('-ege- Query: ' + userLogsQ);
        return Database.getQueryLocator (userLogsQ);
    }

    global void execute(Database.BatchableContext BC, List<ApexLog> scope){

        System.debug('-ege- Query Logs found: ' + scope.size());

        List<QueryCostHistory__c> soqlQueriesFound = new List<QueryCostHistory__c>();

        for (ApexLog log : scope){

            String fullURLRequest = URL.getSalesforceBaseUrl().toExternalForm() + serviceURLEndpoint + log.Id + '/Body';
            String response = callAPI(fullURLRequest);

            for ( String soqlQuery : getSOQLStatementsFromLog(response) ){
                
                QueryCostHistory__c newSOQLStatement = new QueryCostHistory__c( 
                    Query_Statement__c  = soqlQuery,
                    QueryType__c        = 'SOQL Query',
                    Status__c           = 'Analyzed', //we do not want to analyze SOQL Queries
                    Scan_Identifier__c  = scanIdentifier
                );

                soqlQueriesFound.add(newSOQLStatement);
            }
        }
        
        insert soqlQueriesFound;
        System.debug('-ege- soqlQueriesFound: ' + soqlQueriesFound.size() + ' soql Queries to Analyze');
    }

    private String callAPI(String URL){

        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod('GET');
        httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        httpRequest.setEndpoint(URL);
        HttpRequest.setCompressed(true);

        String response = '';

        try {
            Http http = new Http();
            HttpResponse httpResponse = http.send(httpRequest);
            
            if (httpResponse.getStatusCode() == 200 ) {
                response = httpResponse.getBody();

            }else{
                System.debug('-ege- Error calling to retrieve a log');
            }
        }catch( System.Exception e) {
            System.debug('-ege- Callout exception: ' + e);
        }

        return response;
    }

    global void finish(Database.BatchableContext BC){

        System.debug('-ege- Batch finished ' + Datetime.newInstance(System.now().date(), System.now().time()));
    } 
    
    private List<String> getSOQLStatementsFromLog(String fullLog){

        //String queryPattern = '(?m)SELECT[\\s\\S]+?\\s*?$'; 

        List<String> soqlQueriesFound = new List<String>();

        //String queryPattern2 = 'SELECT\\s+?[^\\s]+?\\s+?FROM\\s+?[^\\s]+?\\s+?WHERE.*'; //este funciona con WHERE
        String queryPattern3 = 'SELECT{1}\\s+?[^\\s]+?\\s+?FROM[^SELECT].*';
        
        Pattern p = Pattern.compile(queryPattern3);
        Matcher m = p.matcher(fullLog);

        while ( m.find() ){

            String queryFound = m.group();

            //Queries containing invalid characters are discarded: 
            //SELECT (more)...
            //SELECT adfaf " 
            if (queryFound.contains('"') ||queryFound.contains('(more)')) {
                    System.debug('Query is discarded: ' + queryFound);
            }else{
                soqlQueriesFound.add(queryFound);
            }
        }
        System.debug('-ege- Lista de Queries: ' + soqlQueriesFound);
        return soqlQueriesFound;
    }

    /* private Boolean queryIsOverStdObject( String query){
                    
        Pattern p2 = Pattern.compile('FROM\\s+\\w*');
        Matcher m2 = p2.matcher(query);

        if (m2.find()){
            
            //String FROMClause = m2.group();
            String objectQueried = m2.group().substringAfter('FROM ');

            Schema.DescribeSObjectResult[] descResult = Schema.describeSObjects(new String[]{objectQueried});

            if (descResult[0] != NULL ){
                return (!descResult[0].isCustom());
            }else{
                return false;
            }

        }else{
            return false;    
        }
    } */
}
