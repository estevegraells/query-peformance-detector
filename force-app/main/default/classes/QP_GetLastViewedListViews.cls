/**
 * @author Esteve Graells
 * @date October 2018
 *
 * @description Controller to get the last List Views executed on the period
 */
public with sharing class QP_GetLastViewedListViews {

	//@description Look in the past lookPastNumDays to get List Views executed
	public Integer lookPastNumDays = 0;

	public QP_GetLastViewedListViews(Integer days) {

		Logger.insertLogEntry('Start scanning for Last executed Views ' + Datetime.newInstance(System.now().date(), System.now().time()));
		lookPastNumDays = days;
	}

	//Get Last Viewed List Viewed Run in the last N days
	public String getLastRunViews(){

		String lastExecutedViewsQ =
			' SELECT Id, LastViewedDate' +
			' FROM ListView ' +
			' WHERE LastViewedDate = LAST_N_DAYS:' + lookPastNumDays +
			' ORDER BY LastViewedDate DESC';

		Datetime now = datetime.now();
		String runIdentifier = now.format('YYYYMMDDHHmmss');

		try{
			List<ListView> lastViewsExecuted = Database.query(lastExecutedViewsQ);
			Logger.insertLogEntry(lastViewsExecuted.size() + ' Views to Analyze');

			//Insert those reports as Pending Plans
			List<QueryCostHistory__c> pendingListViewsToAnalyze = new List<QueryCostHistory__c>();
			for (ListView l: lastViewsExecuted) {

				QueryCostHistory__c newPendingReport = new QueryCostHistory__c(
					Query_ID__c = l.Id,
					QueryType__c = 'ListView Query',
					Status__c = 'Pending',
					Scan_Identifier__c = runIdentifier
					);

				pendingListViewsToAnalyze.add(newPendingReport);
			}

			insert pendingListViewsToAnalyze;

		}catch (Exception e) {
			System.debug( '-ege- Exception: ' + e.getMessage() );
		}

		return runIdentifier;
	}
}
